**ServiceNow - Service Portal Widget - Tracker Button**

**Overview:**

This widget is in place to serve as a button and provide record count information.

Example: The button is placed on a Homepage and labled 'IT',using this widgets
functionaility you can now select the IT ticket table and display an active
record count of IT related tickets (Or a table of your choice).

![](tracker-button.png)

**How can I use this widget?**
 
 **1.** Download the XML linked to this project and import it into your ServiceNow Instance or alternatively, copy and paste each of the files above in the widget code sections (html,client,server,css).
 
 **2.** Access the page designer, search for the widget in the filter navigator.
 
 **3.** Drag and drop the widget on the page (ideally within a 25%/3 width column.)
 
 **4.** Configure the widget to your requirement. Check below for configuration help.
 

**How can I configure the widget once it's on the page?**

**Type** - Where would you like to send the user? The widget currently supports: Pages, URLS, Catalogue Catagories, Catalogue Items, KB Topics, KB Articles and KB Catagories. After selecting a data type another choice field will appear allowing you to input an specific location.

**Table 1 & 2** - Input the table name you would like to pull records from. For example, selecting 'Incident' will pull records from the incident table.

**Condition For Table 1 & 2** - This string field accepts Encoded Querys only. For example: **'active=true'** Read more here: https://docs.servicenow.com/bundle/newyork-platform-user-interface/page/use/using-lists/concept/c_EncodedQueryStrings.html

**Tracker Name 1 & 2** - This will be the text displayed before your count.

![](tracker-menu.png)

**FAQ:**

**Q:** How can I change the color and size effect?

**A:** You'll have to change the CSS youself, this can be done in the widget CSS section(Best option) or in the page specific CSS section. I was too lazy to add this to the option schema.   Learn more here: https://www.w3schools.com/css/css_colors.asp

**If you are still stuck email me at:** jordanthompson730@gmail.com

