(function(){
	var gr = $sp.getInstanceRecord();
	data.href = $sp.getMenuHREF(gr);
	data.target = options.target || "";
	
	
	
	// TRACKER 1
	
	var table1 = options.table_1; //user input for table 1

	if(table1 != ''){
	var table1query = new GlideRecord('sys_db_object');
	table1query.addQuery('sys_id', table1);
	table1query.query();
	var table1name = '';
	
	if (table1query.next()) {
		table1name = table1query.getValue('name');
	}
	
	var query1 = options.query_1; //for encoded query on table 1
				
	data.goodcount = '';
	data.badcount = '';
	

	var incident = new GlideAggregate(table1name); // opens incident table
	incident.addEncodedQuery(query1); // caller is me and incident is active
	incident.addAggregate("COUNT"); // count the records from the above condition.
	incident.query(); // run the query on the server
	var answer = ''; // data object where we will place our count.
	
	if(incident.next()) {
		answer = incident.getAggregate("COUNT"); // set our var the amount of records counted.
			
if (answer > 0) // if our count is greater than 0
	{
		data.badcount = answer; // set our data object for the client to our count
		data.goodcount = -1; // allows the html for goodcount not to be triggered
	}
		else if (answer == 0) // if our count is 0
			{
				data.goodcount = answer; // set our data object for the client to our count
			}
	}
	}
		
		
		
		
		
		
		
		
	//TRACKER 2
		
	
	var table2 = options.table_2; //user input for table 2
	
	if(table2 != '') {
	var table2query = new GlideRecord('sys_db_object');
	table2query.addQuery('sys_id', table2);
	table2query.query();
	var table2name = '';
	
	if (table2query.next()) {
		table2name = table2query.getValue('name');
	}
	
  var query2 = options.query_2; //for encoded query on table 2
	
	data.requestcount = '';
	data.requestcount2 = '';
	
	var request = new GlideAggregate(table2name);
	request.addEncodedQuery(query2);
	request.addAggregate("COUNT");
	request.query();
	var answer2 = '';
	
	if(request.next()) {
		answer2 = request.getAggregate("COUNT");
		
		if (answer2 > 0)
			{
				data.requestcount = answer2;
				data.requestcount2 = -1;
			}
		else if (answer2 == 0)
						 {
						 data.requestcount2 = answer2;
						 }
	}
	}
	
	
})();